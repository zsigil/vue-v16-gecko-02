# dashy

This is a Vue application created by Vue CLI 3 (Babel, Vuex, node-sass, eslint preinstalled).

### Added packages

* style-resources-loader (for global.scss use)
* vuetify (for general styling, grid, widgets)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
