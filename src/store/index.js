import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import axios from 'axios';
export default new Vuex.Store({
  state: {
    selectedWidget : {dashID:null, widgetID:null},
    timezoneinfo: [],
    dashboards: [
      {
        id: 0,
        title: 'First Dashboard',
        active: false,
        widgets: [
          {
            id: 0,
            type: 'text',
            text: 'I am text for first dash',
            link: ''
          },
          {
            id: 1,
            type: 'link',
            link: 'https://vuetifyjs.com/en/components/grids',
            text: null,
            title: null,
          }
        ]
      },
      {
        id: 1,
        title: 'Second Dashboard',
        active: true,
        widgets: [
          {
            id: 0,
            type: 'text',
            title: 'I am text for second dash',
            text: 'I am text for second dash',
            timezone: null,
          },
          {
            id: 1,
            type: 'link',
            title: 'Link widget',
            text: 'https://vuetifyjs.com/en/components/grids',
            timezone: null,
          },
          {
            id: 2,
            type: 'timezone',
            title: 'Jack',
            timezone: 'Pacific/Majuro',
            utcoffset: '12:00'
          }
        ]
      },
    ]
  },
  getters: {
    getActiveDashBoard : (state) => state.dashboards.filter(d=>d.active===true)[0],
    getIdOfDashBoardFromArrayPosition: (state) => (pos) => {
      return state.dashboards[pos].id
    },
    timezoneList: state => {
      let timezonelist = []
      for (var i = 0; i < state.timezoneinfo.length; i++) {
        timezonelist.push(state.timezoneinfo[i].timeZone.ianaTimeZoneId)
      }
      return timezonelist;
    },
    getOffsetFromZoneId: ( state) => zoneName => {
      let timezone = state.timezoneinfo.filter(t=>t.timeZone.ianaTimeZoneId===zoneName)[0];
      return timezone.timeZone.utcOffset;
    }
  },
  mutations: {
    setActiveDashTitle(state, payload){
      state.dashboards.filter(d=>d.id==payload.id)[0].title = payload.title;
    },
    changeActiveDashboard(state,dashID){
      state.dashboards.forEach(d=>d.active = false); //first deactivate all dashboards
      state.dashboards.filter(d=>d.id==dashID).forEach(d=>d.active=true)
    },
    addNewDashboard(state, newID){
      state.dashboards.push({
          id: newID,
          title: 'New Dashboard',
          active: false,
          widgets: [],
      })
    },
    addWidget(state, dashboardID){
      state.dashboards.filter(d=>d.id==dashboardID)[0].widgets.push({
        id: (new Date()+Math.random()).toString(),
      })
    },
    deleteWidget(state, payload){
      state.dashboards.filter(d=>d.id==payload.dashID)[0].widgets = state.dashboards.filter(d=>d.id==payload.dashID)[0].widgets.filter(w=>w.id !== payload.widgetID)
    },
    selectWidget(state, payload){
      state.selectedWidget.dashID = payload.dashID;
      state.selectedWidget.widgetID = payload.widgetID;
    },
    unselectWidget(state){
      state.selectedWidget.dashID = null;
      state.selectedWidget.widgetID = null;
    },
    changeWidget(state, widgetinfo){
      let dashID = state.selectedWidget.dashID;
      let widgetID = state.selectedWidget.widgetID;
      let dash = state.dashboards.filter(d=>d.id==dashID)[0]
      let widget = dash.widgets.filter(w=>w.id==widgetID)[0]
      widget.type = widgetinfo.type;
      widget.link = widgetinfo.link;
      widget.text = widgetinfo.text;
      widget.imagelink = widgetinfo.imagelink;
      widget.title = widgetinfo.title;
      widget.timezone = widgetinfo.timezone;
      widget.todos = widgetinfo.todos;
      widget.utcoffset = widgetinfo.utcoffset;
      state.selectedWidget = {dashID:null, widgetID:null}
    },
    editWidget(state, widgetinfo){
      let dashID = state.selectedWidget.dashID;
      let widgetID = state.selectedWidget.widgetID;
      let dash = state.dashboards.filter(d=>d.id==dashID)[0]
      let widget = dash.widgets.filter(w=>w.id==widgetID)[0]
      widget.type = widgetinfo.type;
      widget.link = widgetinfo.link;
      widget.text = widgetinfo.text;
      widget.imagelink = widgetinfo.imagelink;
      widget.title = widgetinfo.title;
      widget.timezone = widgetinfo.timezone;
      widget.todos = widgetinfo.todos;
      widget.utcoffset = widgetinfo.utcoffset;
      state.selectedWidget = {dashID:null, widgetID:null}
    },
    setTimeZoneList(state,timezoneinfo){
      state.timezoneinfo = timezoneinfo;
    }

  },
  actions: {
    setActiveDashTitle({commit}, payload){
      commit('setActiveDashTitle', payload)
    },
    changeActiveDashboard({commit}, dashID){
      commit('changeActiveDashboard', dashID)
    },
    addNewDashboard({commit, dispatch}){
      let newID = (new Date(Date.now())).toString()
      commit('addNewDashboard', newID)
      dispatch('changeActiveDashboard', newID)
    },
    addWidget({commit}, dashboardID){
      commit('addWidget', dashboardID)
    },

    deleteWidget({commit}, payload){
      commit('deleteWidget', payload)
    },
    selectWidget({commit}, payload){
      commit('selectWidget', payload)
    },
    unselectWidget({commit}){
      commit('unselectWidget')
    },
    changeWidget({commit}, widgetinfo){
      commit('changeWidget', widgetinfo)
    },
    editWidget({commit}, widgetinfo){
      commit('editWidget', widgetinfo)
    },
    setTimeZoneInfo({commit}){
      let APIKEY = process.env.VUE_APP_MICROSOFT_TIMEZONE_APIKEY;
      axios.get(`https://dev.virtualearth.net/REST/v1/TimeZone/List/?timezonestandard=IANA&key=${APIKEY}`)
      .then(res=>{
        let timezones= res.data.resourceSets[0].resources;
        commit('setTimeZoneList', timezones)
      })
      .catch(e=>{
        alert(e)
      })
    }
  },
  modules: {
  }
})
